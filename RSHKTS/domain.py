__author__ = 'horacio'
from RSHKTS import models, serializers
import random
from datetime import datetime

EXPIRED_EXAM_ERROR_CODE = 'e_100'


def get_candidate(candidate_name, id_number):

    candidate_query = models.Candidate.objects(name=candidate_name, id_number=id_number)
    if len(candidate_query) > 0:
        candidate = candidate_query.get(0)
        candidate.existing = True

    else:
        candidate = models.Candidate(name=candidate_name, id_number=id_number).save()
        candidate.existing = False

    return candidate


def get_test(template_id, candidate_id):

    if candidate_id is None:
        raise ValueError("Invalid request arguments: template_id, candidate_id", template_id, candidate_id)

    else:

        template = models.TestTemplate.objects(id=template_id).get(0)
        candidate = models.Candidate.objects(id=candidate_id).get(0)

        test = models.Test.objects(candidate=candidate, template=template)

        if len(test) > 0:
            test = test.get(0)
            if test.state != 'a':
                raise ValueError("Test already finished", test.last_update)

            elif ((datetime.now() - test.dateTime).days * 24 * 60) > test.template.duration:
                final_score = get_final_score(test, test.chosen_answers)
                update_candidate_templates_status(test.candidate, test.template, "f")
                test.update(set__state='f',
                            set__final_score=final_score)
                raise ValueError("Test already finished", test.last_update)

            test = delete_exam_data(test, test.state)
            return test

        else:
            questions = []
            answers = []
            questions_to_check = []

            for topic in template.question_topics:
                topic_questions = models.Question.objects(topics={'description': topic.description})
                # generates an array with random numbers
                question_indexes = random.sample(range(0, len(topic_questions)), template.questions_per_topic)
                for question_number in question_indexes:
                    questions.append(topic_questions[question_number])
                    answers.append(-1)  # fill the answers with -1 (default not chosen answer)

            candidate.update(add_to_set__current_test_templates=models.TemplatesStatus(test_template=template,state="a"))
            test = models.Test(candidate=candidate, template=template, questions=questions, chosen_answers=answers,
                                questions_to_check=questions_to_check).save()
            test = delete_exam_data(test, test.state)
            return test


def update_test(test_id, answers, state, questions_to_check):

    if test_id is None or answers is None or state is None:
        raise ValueError("Invalid request parameters supplied : test_id, answers, state",
                         test_id, answers, state)

    test = models.Test.objects(id=test_id).get(0)

    # raise an error when the test has expired
    if (((datetime.now() - test.dateTime).days * 24 * 60) > test.template.duration) & (test.state == 'a'):
        update_candidate_templates_status(test.candidate, test.template, "f")
        final_score = get_final_score(test, answers)
        test.update(set__chosen_answers=answers,
                    set__last_update=datetime.now(),
                    set__state="f",
                    set__final_score=final_score,
                    set__questions_to_check=questions_to_check)
        test.reload()
        raise ValueError("El examen se encuentra con estado finalizado", EXPIRED_EXAM_ERROR_CODE, test.last_update)

    # raise an error when the test has the state "finalized"
    if test.state == "f":
        raise ValueError("El examen se encuentra con estado finalizado", EXPIRED_EXAM_ERROR_CODE, test.last_update)

    final_score = 0
    if state == "f":
        update_candidate_templates_status(test.candidate, test.template, "f")
        final_score = get_final_score(test, answers)

    test.update(set__chosen_answers=answers,
                set__last_update=datetime.now(),
                set__state=state,
                set__final_score=final_score,
                set__questions_to_check=questions_to_check)
    test.reload()
    test = delete_exam_data(test, state)
    return test


def delete_exam_data(test, state):
    if state != "f":
        del test.final_score
    return test


def get_final_score(test, answers):
    score = 0
    questions = test.questions
    score_per_question = 100/len(answers)
    for index in range(len(answers)):
        selected_answer = answers[index]
        if questions[index].index_correct_answer == int(selected_answer):
            score += score_per_question
    return score


def update_candidate_templates_status(candidate, template, state):
    current_test_templates = candidate.current_test_templates
    for template_index in range(len(current_test_templates)):
        if current_test_templates[template_index].test_template == template:
            current_test_templates[template_index].state = state

    candidate.update(current_test_templates = current_test_templates)