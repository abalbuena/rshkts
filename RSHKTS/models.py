import datetime
from mongoengine import *

#database connection

connect("RSHKTSDB")

# database fields
# EmbeddedDocuments doesnt necessarily needs to be stored in a Collection -
# - as ReferenceFields, they are just "Embedded" in other documents


class Image(EmbeddedDocument):
    name = StringField(max_length=50)
    data = StringField()


class Topic(EmbeddedDocument):
    description = StringField(max_length=100)


class Answer(EmbeddedDocument):
    description = StringField()
    image = EmbeddedDocumentField(Image)


class QuestionStatement(EmbeddedDocument):
    text = StringField()
    code = StringField(max_length=50)
    image = EmbeddedDocumentField(Image)


class Question(Document):#mins
    statement = EmbeddedDocumentField(QuestionStatement)
    text = StringField()
    image = EmbeddedDocumentField(Image)
    topics = ListField(EmbeddedDocumentField(Topic))
    answers = ListField(EmbeddedDocumentField(Answer))
    index_correct_answer = IntField(default=0)

# one template may have more than one topic to select answers


class TestTemplate(Document):
    description = StringField(max_length=50)
    question_quantity = IntField(default=10)
    question_topics = ListField(EmbeddedDocumentField(Topic))
    questions_per_topic = IntField(default=5)
    duration = IntField(default=120)  #mins


class TemplatesStatus(EmbeddedDocument):
    test_template = ReferenceField(TestTemplate)
    state = StringField()


class Candidate(Document):
    name = StringField(max_length=50)
    id_number = StringField(max_length=11)
    current_test_templates = ListField(EmbeddedDocumentField(TemplatesStatus), default=[])


class Test(Document):
    candidate = ReferenceField(Candidate)
    questions = ListField(ReferenceField(Question))
    template = ReferenceField(TestTemplate)
    dateTime = DateTimeField(default=datetime.datetime.now())
    final_score = FloatField(default=0)
    chosen_answers = ListField(IntField(default=-1))
    state = StringField(default='a')
    last_update = DateTimeField()
    questions_to_check = ListField(IntField())

