from RSHKTS import models
from RSHKTS.queries import images

# Logica L1
#1
models.Question(
    text="El sistema de campeonato de ”Eliminacíon directa” funciona como sigue: "
         "El torneo se divide en rondas. Por cada ronda, se emparejan a todos los concursantes, y solo los que ganan "
         "sus respectivos encuentros, avanzan a la siguiente ronda. Los eliminados, quedan fuera del torneo. "
         "Este proceso para eliminar al siguiente grupo de concursantes, hasta que queda uno solo que es declarado "
         "campeón. Se organiza un torneo de ajedrez con 128 participantes. "
         "Cuántos partidos debe ganar un jugador para salir campeón?",
    topics=[models.Topic(description="logica")],
    answers=[models.Answer(description="4"),
             models.Answer(description="6"),
             models.Answer(description="7"),
             models.Answer(description="9"),
             models.Answer(description="No se puede determinar antes de que empiece el torneo")
             ],
    index_correct_answer=2
).save()

#2
models.Question(
    text="Definición: Una palabra ”UCOMERA”, está definida como aquella que tiene un número "
         "primo mayor que 2 de letras, y además, tiene la misma cantidad de vocales que consonantes."
         "Considerar el siguiente texto de Gabriel García Márquez: \n "
         "”Sin embargo, antes de llegar al verso final ya había comprendido que no saldría jamás de ese "
         "cuarto, pues estaba previsto que la ciudad de los espejos (o los espejismos) sería arrasada por el "
         "viento y desterrada de la memoria de los hombres en el instante en que Aureliano Babilonia "
         "acabara de descifrar los pergaminos, y que todo lo escrito en ellos era irrepetible desde siempre "
         "y para siempre, porque las estirpes condenadas a cien años de soledad no tenían una segunda "
         "oportunidad sobre la tierra.” \n Cuántas palabras ”UCOMERAS” hay en el texto citado?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="8"),
        models.Answer(description="Ninguna"),
        models.Answer(description="9"),
        models.Answer(description="10"),
        models.Answer(description="11")
    ],
    index_correct_answer=1
).save()

#3
models.Question(
    text="Definicion: Considerar la siguiente secuencia de símbolos: 69++96XX69++96XX69++?6X? "
         "Qué vendría en reemplazo de los signos de interrogación?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="6X"),
        models.Answer(description="96"),
        models.Answer(description="9X"),
        models.Answer(description="+6"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=2
).save()

#4
models.Question(
    text="Definicion: Considerar la siguiente secuencia de símbolos: #+++---#++---#++--#+--#+-#?? "
         "Qué vendría en reemplazo de los signos de interrogación?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description=" #-"),
        models.Answer(description="##"),
        models.Answer(description="+#"),
        models.Answer(description="-#"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=3
).save()

#5
models.Question(
    text="Se escriben los numeros del 3 al 17 en una línea. Si sumamos, tomando de a dos los números consecutivos "
         "(que aparecen uno al lado del otro) podemos concluir que:",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="La suma siempre es impar"),
        models.Answer(description="La suma siempre es par"),
        models.Answer(description="La suma siempre es un número primo"),
        models.Answer(description="La suma es, en algunos casos par y en otros impar"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=0
).save()

#6
models.Question(
    text="Cuál es el siguiente número de la secuencia: 1, 2, 10, 37, 101, ...",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="125"),
        models.Answer(description="64"),
        models.Answer(description="165"),
        models.Answer(description="126"),
        models.Answer(description="No hay ninguna lógica en la secuencia")
    ],
    index_correct_answer=4
).save()

#7
models.Question(
    text="Al tirar dos dados de 6 caras (las caras tienen los valores 1, 2, 3, 4, 5 y 6) es mas probable que "
         "la suma de los dados sea: ",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="12"),
        models.Answer(description="2"),
        models.Answer(description="4"),
        models.Answer(description="8"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=4
).save()

#8
models.Question(
    text="La probabilidad de tirar una moneda y que caiga cruz es 0.5. Si tiramos una moneda y sale cruz, "
         "cuál es la probabilidad de que tiremos la mismamoneda y vuelva a salir cruz? ",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="La misma: 0.5"),
        models.Answer(description="La probabilidad es mayor"),
        models.Answer(description="La probabilidad es menor"),
        models.Answer(description="Cuando se tira una moneda, ya no se puede volver a calcular la probabilidad"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=0
).save()

#9
models.Question(
    text="En un grupo de cuatro equipos que juegan todos contra todos, la tabla de posiciones es la siguiente: \n "
         "[{image}] \n "
         "Donde: \n "
         "PJ Partidos Jugados \n "
         "PG Partidos Ganados \n "
         "PE Partidos Empatados \n "
         "PP Partidos Perdidos \n "
         "GF Goles a favor \n "
         "GC Goles en contra \n "
         "Pts Puntos \n "
         "En la última fecha se enfrentan Argentina vs. Suecia y Brasil vs. Inglaterra. "
         "Al final de jugar todos contra todos, clasifican 2 equipos a la siguiente etapa de acuerdo a los "
         "siguientes criterios en orden: \n "
         "(a) Mayor cantidad de puntos \n "
         "(b) Mayor saldo de goles: GF − GC \n "
         "(c) Mayor cantidad de goles a favor \n "
         "(d) Sorteo \n "
         "Se puede concluir que:",
    topics=[models.Topic(description="logica")],
    image=models.Image(name="l1p9", data=images.img_l1_question9),
    answers=[
        models.Answer(description="Argentina y Brasil ya están clasificados"),
        models.Answer(description="Solo Argentina está clasificada por tener el mayor saldo de goles"),
        models.Answer(description="Inglaterra está eliminada por tener el menor saldo de goles"),
        models.Answer(description="Inglaterra, ganando por cualquier diferencia en la última fecha, clasifica a la siguiente ronda"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=4
).save()

#10
models.Question(
    text="Un juego es considerado de ”duración teórica infinita” cuando, podría darse "
         "el caso que nunca termine.La siguiente lista de deportes es de ”duración "
         "teórica infita”:",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Fútbol, Volleyball, Basketball"),
        models.Answer(description="Tenis, Volleball, Basketball"),
        models.Answer(description="Fútbol, Basketball, Tenis"),
        models.Answer(description="Fútbol y Basketbal"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=1
).save()

#11
models.Question(
    text="Hay cinco personas con diferentes alturas. Alan es más alto que Alberto "
         "quien es más alto que Eugenio. Carla es más baja que Guillermo, pero más "
         "alta que Alan. Quién es la tercera persona más alta?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Guillermo"),
        models.Answer(description="Alberto"),
        models.Answer(description="Carla"),
        models.Answer(description="Eugenio"),
        models.Answer(description="Alan")
    ],
    index_correct_answer=4
).save()

#12
models.Question(
    text="Cuál es el siguiente número de la secuencia: 1, 3, 7, 15, 31, 63, 127, ____",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="255"),
        models.Answer(description="250"),
        models.Answer(description="260"),
        models.Answer(description="245"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=0
).save()

#13
models.Question(
    text="Amanda, Sara y Néstor comieron cada uno algo diferente en el desayuno. "
         "Uno comió tostadas, otro comió salchichas y el otro comió melón."
         "Si sabemos que: \n "
         "(a) A Amanda le gusta comer melón o tostadas para el desayuno. \n "
         "(b) Solo Sara y Nétor comen algún tipo de carne en el desayuno. \n "
         "(c) Néstor no comió salchichas ni tostadas en el desayuno. \n "
         "Qué comió cada uno en el desayuno?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Amanda Tostadas/Sara Salchichas/Néstor Melón"),
        models.Answer(description="Amanda Salchichas/Sara Tostadas/Néstor Melón"),
        models.Answer(description="Amanda Tostadas/Sara Melón/Néstor Salchichas"),
        models.Answer(description="Amanda Melón/Sara Tostadas/Néstor Salchichas"),
        models.Answer(description="Amanda Melón/Sara Salchichas/Néstor Tostadas")
    ],
    index_correct_answer=0
).save()

#14
models.Question(
    text="Considerar las siguientes proposiciones: \n "
         "(a) A David le gusta participar en toda clase de deportes. Pero como algunos "
         "deportes son peligrosos, siempre que David participa en algún deporte, está "
         "en peligro. \n "
         "(b) Tomás es mayor que Eduardo. Por lo tanto, Tomás no puede ser mayor que "
         "Roberto ya que Roberto es menor que Eduardo. \n "
         "(c) Es peligroso volar en algunos aviones. Como este es una avión de carga, "
         "debe ser peligroso volar en este avión. ",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Todas las proposiciones son verdaderas"),
        models.Answer(description="Solo la proposición (a) es verdadera"),
        models.Answer(description="Solo la proposición (b) es verdadera"),
        models.Answer(description="Todas las proposiciones son falsas"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=3
).save()

#15
models.Question(
    text="Considerar las siguientes proposiciones: \n "
         "(a) El agua fluirá hacia el tanque: \n "
         "  • Cuando estén abiertas las válvulas A y B al mismo tiempo \n "
         "  • O bien, cuando estén abiertas las válvulas C, D y E al mismo tiempo \n "
         "  • O bien, cuando estén abiertas todas las válvulas al mismo tiempo \n "
         "Por lo tanto, cuando sólo las válvulas A, C y E estén abiertas al mismo tiempo,"
         "el agua no fluirá hacia el tanque.\n "
         "(b) Para encender una bombilla hay dos interruptores, el A y el B, cada uno de ellos con "
         "dos posiciones: SI y NO. La bombilla se enciende: \n "
         "  • Si el interruptor A, pero no el interruptor B, está en la posición SI \n "
         "  • O bien, si el interruptor B, pero no el interruptor A, está en la posición SI. \n "
         "Por lo tanto, la bombilla no encenderá si el interruptor A, o el interruptor B, pero no "
         "ambos, estén en posicin NO.",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Todas las proposiciones son verdaderas"),
        models.Answer(description="Solo la proposición (a) es verdadera"),
        models.Answer(description="Solo la proposición (b) es verdadera"),
        models.Answer(description="Todas las proposiciones son falsas"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=1
).save()

#tito-statement
tito_statement = models.QuestionStatement(
    code="tito_statement",
    text="TITO el roboTITO acepta únicamente la siguiente lista de comandos. \n "
         "avanzar n Avanza n metros para adelante \n "
         "girar n Gira n grados en el mismo sentido de las manecillas del reloj \n "
         "ngirar n Gira n grados en sentido opuesto al de las manecillas del reloj \n "
         "Tito está ubicado en el siguiente plano: \n "
         "[{image}] \n "
         "Entre un cuadrado y otro, hay 1 metro de distancia. Inicialmente,tito ”apunta” hacia "
         "el norte (arriba). Si los comandos están en una lista separada con con punto y coma (;), "
         "Tito los ejecuta en secuencia ordenada. Así por ejemplo, una secuencia de comandos que "
         "llevaría a Tito desde la posición inicial hasta la salida (marcada con una ”S”) es: "
         "avanzar 3; girar 90; avanzar 3 \n"
         "Además de esto, se sabe que tito avanza 1 metro por cada segundo, y cada giro que realiza, "
         "le cuesta 2 segundos de tiempo. ",
    image =  models.Image(name="l1p16", data=images.img_l1_question16)
)


#16
models.Question(
    statement = tito_statement,
    text="Cuál de las siguientes instrucciones translada a TITO desde su posición inicial hasta la salida?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="avanzar 3; girar 90; avanzar 3"),
        models.Answer(description="avanzar 2; girar 90; avanzar 3; ngirar 90; avanzar 1"),
        models.Answer(description="avanzar 1; girar 90; avanzar 1;ngirar 90; avanzar 2"),
        models.Answer(description="a, b y c son respuestas correctas"),
        models.Answer(description="a y b son respuestas correctas")
    ],
    index_correct_answer=4
).save()

#17
models.Question(
    statement = tito_statement,
    text="Cuál de los siguientes comandos harán que TITO salga de los límites de la figura?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="avanzar 3; girar 90; avanzar 3"),
        models.Answer(description="avanzar 2; girar 90; avanzar 3; ngirar 90; avanzar 1"),
        models.Answer(description="avanzar 1; girar 90; avanzar 1;ngirar 90; avanzar 2"),
        models.Answer(description="a, b y c son respuestas correctas"),
        models.Answer(description="a y b son respuestas correctas")
    ],
    index_correct_answer=2
).save()

#18
models.Question(
    statement = tito_statement,
    text="Cuál es la lista de comandos que más rápidamente translada a Tito desde la posición inicial hasta la salida?",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="avanzar 3; girar 90; avanzar 3"),
        models.Answer(description="girar 45; avanzar 3√2"),
        models.Answer(description="avanzar 1; girar 45; avanzar 1; ngirar 45; avanzar 1; girar 45; avanzar 1; "
                                  "ngirar 45; avanzar 1; girar 45; avanzar 1"),
        models.Answer(description="girar 90; avanzar 3; ngirar 90;avanzar 3"),
        models.Answer(description="Todos tardan la misma cantidad de tiempo")
    ],
    index_correct_answer=1
).save()

#19
models.Question(
    statement = tito_statement,
    text="Cuántos segundos tarda TITO en ejecutar la siguiente secuencia de comandos: avanzar 1; girar 45; \n  "
         "avanzar 1; ngirar 45; avanzar 1; girar 45; avanzar 1; ngirar 45; avanzar 1; girar 45; avanzar 1",
    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="12"),
        models.Answer(description="14"),
        models.Answer(description="8"),
        models.Answer(description="16"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=3
).save()

#20
models.Question(
    text="Chandler, Monica, Phoebe, Rachel, y Joey tienen uno de los siguientes trabajos: analista, actor, jefe de modas,"
         " masajista, y cocinero. Sus salarios son $51900, $62300, $57400, $67500, y $60900. Cuál es el salario y "
         "el trabajo de cada persona si: \n"
         "(a) El jefe de modas gana $57400. \n"
         "(b) El masajista gana $60900. \n"
         "(c) El actor gana más que el analista. \n"
         "(d) Phoebe no es jefa de modas ni analista. \n"
         "(e) Rachel no es analista ni jefe de modas. \n"
         "(f) El masajista gana más que el jefe de modas. \n"
         "(g) El analista no gana $62300. \n"
         "(h) El jefe de modas gana más que el analista. \n"
         "(i) Chandler no es actor ni cocinero. \n"
         "(j) Joey no es analista ni actor. \n"
         "(k) El actor gana más que el jefe de modas. \n"
         "(l) El actor no gana $60900. \n"
         "(m) El cocinero no gana $60900. \n"
         "(n) Rachel no es cocinero ni actor. \n"
         "(o) El cocinero gana más que el jefe de modas. \n"
         "(p) Monica no es masajista ni jefa de modas. \n"
         "(q) El actor gana más que el cocinero. \n"
         "(r) Joey no es jefe de modas ni masajista.",

    topics=[models.Topic(description="logica")],
    answers=[
        models.Answer(description="Chandler/Jefe de Modas/$57400, "
                                  "Monica/Analista/$51900, "
                                  "Phoebe/Actor/$62300, "
                                  "Rachel/Masajista/$60900, "
                                  "Joey/Cocinero/$67500"),
        models.Answer(description="Chandler/Jefe de Modas/$57400, "
                                  "Monica/Analista/$51900, "
                                  "Phoebe/Masajista/$67500, "
                                  "Rachel/Actor/$60900, "
                                  "Joey/Cocinero/$62300"),
        models.Answer(description="Chandler/Jefe de Modas/$57400, "
                                  "Monica/Analista/$51900, "
                                  "Phoebe/Actor/$67500, "
                                  "Rachel/Masajista/$60900, "
                                  "Joey/Cocinero/$62300"),
        models.Answer(description="Chandler/Analista/$57400, "
                                  "Monica/Cocinero/$51900, "
                                  "Phoebe/Masajista/$67500, "
                                  "Rachel/Jefe de Modas/$60900, "
                                  "Joey/Actor/$62300"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=2
).save()

models.TestTemplate(description="Logica L1",
                    question_quantity=20,
                    question_topics=[models.Topic(description="logica")],
                    questions_per_topic=20).save()

