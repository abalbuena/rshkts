from RSHKTS import models
from RSHKTS.queries import images

#Matematica M1
#1
models.Question(
    topics=[models.Topic(description='matematica')],
    text="5/4 + 3/2 + 10/8 = ",
    answers=[models.Answer(description="1/4"),
             models.Answer(description="3/4"),
             models.Answer(description="4/3"),
             models.Answer(description="4"),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=3
).save()

#2
models.Question(
    topics=[models.Topic(description='matematica')],
    text="La división entera y su correspondiente resto de 358 ÷ 9 son:",
    answers=[models.Answer(description="9 y 9"),
               models.Answer(description="39 y 7"),
               models.Answer(description="7 y 39"),
               models.Answer(description="38 y 7"),
               models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=1
).save()

#3
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente expresion: [{image}] \n "
         "La expresión es numéricamente equivalente a:",
    image=models.Image(name="m1p3", data=images.img_m1_question3),
    answers=[models.Answer(description="n + 1"),
             models.Answer(description=" 2 * n"),
             models.Answer(description="((n+1) * n) / 2"),
             models.Answer(description="i"),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=2
).save()

#4
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Si x*y es distinto de 0, entonces (1-x)/x*y = ",
    answers=[models.Answer(description="(1/xy) - (1/y)"),
             models.Answer(description="(x/y) - (1/x)"),
             models.Answer(description="(1/xy) - 1"),
             models.Answer(description="(1/xy) - (x²/y)"),
             models.Answer(description="(1/x) - (1/y)")],
    index_correct_answer=0
).save()

#5
models.Question(
    topics=[models.Topic(description='matematica')],
    text="(x²−2x+1)/(x-1) = ",
    answers=[models.Answer(description="x+1"),
             models.Answer(description="x-1"),
             models.Answer(description="(x-1)²"),
             models.Answer(description="(x+1)²/(x-1)"),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=1
).save()

#6
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Si (p-q)/p = 2/7, entonces q/p =  ",
    answers=[models.Answer(description="2/5"),
             models.Answer(description="5/7"),
             models.Answer(description="1"),
             models.Answer(description="7/5"),
             models.Answer(description="7/2")],
    index_correct_answer=1
).save()

#7
models.Question(
    topics=[models.Topic(description='matematica')],
    text="El valor de  [{image}]  es:",
    image=models.Image(name="m1p7", data=images.img_m1_question7),
    answers=[models.Answer(image=models.Image(name="m1p7r1", data=images.img_m1_question7_ans1), description="[{image}]"),
             models.Answer(image=models.Image(name="m1p7r2", data=images.img_m1_question7_ans2), description="[{image}]"),
             models.Answer(image=models.Image(name="m1p7r3", data=images.img_m1_question7_ans3), description="[{image}]"),
             models.Answer(image=models.Image(name="m1p7r4", data=images.img_m1_question7_ans4), description="[{image}]"),
             models.Answer(image=models.Image(name="m1p7r5", data=images.img_m1_question7_ans5), description="[{image}]")],
    index_correct_answer=4
).save()

#8
models.Question(
    topics=[models.Topic(description='matematica')],
    text=" El valor del determinante de  [{image}]  es:",
    image=models.Image(name="m1p8", data=images.img_m1_question8),
    answers=[models.Answer(description="1"),
             models.Answer(description="-1"),
             models.Answer(description="0"),
             models.Answer(description="2"),
             models.Answer(description="-2")],
    index_correct_answer=2
).save()

#9
models.Question(
    topics=[models.Topic(description='matematica')],
    text=" El valor del determinante de  [{image}]  es:",
    image=models.Image(name="m1p9", data=images.img_m1_question9),
    answers=[models.Answer(description="1"),
             models.Answer(description="-1"),
             models.Answer(description="0"),
             models.Answer(description="2"),
             models.Answer(description="-2")],
    index_correct_answer=0
).save()

#10
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente ecuación: x − 2 + 8x = 9y + 3x. La solución de la misma es:",
    answers=[models.Answer(description="x = 0"),
             models.Answer(description="x = 0, y = 1"),
             models.Answer(description="x = −2, y = 9"),
             models.Answer(description="x = 6, y = 9"),
             models.Answer(description="No se puede determinar")],
    index_correct_answer=4
).save()


#11
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente ecuación: x² + 5x − 4 = 0. La solución de la misma es:",
    answers=[models.Answer(description="x = (5±√105) / 2"),
             models.Answer(description="x = (-5±√(85)) / 2"),
             models.Answer(description="x = (-5±√(32)) / 2"),
             models.Answer(description="x = (-5±√(41)) / 2"),
             models.Answer(description="La ecuación no tiene solución")],
    index_correct_answer=3
).save()

#12
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar el siguiente sistema de ecuaciones: \n"
         "x + 2y = 4 \n"
         "x − 2y = 0 \n"
         "La solución de la misma es:",
    answers=[models.Answer(description="x = 1, y = 2"),
             models.Answer(description="x = 2, y = −1"),
             models.Answer(description="x = −1, y = −1"),
             models.Answer(description="x = 2, y = 1"),
             models.Answer(description="El sistema de ecuaciones no tiene solución")],
    index_correct_answer=3
).save()


#13
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar el siguiente sistema de ecuaciones: \n"
         "x² + 2x + 1 = y \n"
         "3y + 5 = y + 9 \n"
         "La solución de la misma es:",
    answers=[models.Answer(description="x = 2, y = −1"),
             models.Answer(description="x = −1, y = 2"),
             models.Answer(description="x = −1 ±√2, y = 2"),
             models.Answer(description="x = −2 ±√8, y = 2"),
             models.Answer(description="El sistema de ecuaciones no tiene solución")],
    index_correct_answer=2
).save()


#14
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar las siguientes ecuaciones de recta: \n"
         "y = 3x + 8 \n"
         "y = x + 1 \n"
         "La rectas se intersectan en el punto:",
    answers=[models.Answer(description="( (7/2), (5/2) )"),
             models.Answer(description="( -(9/4), -(5/4) )"),
             models.Answer(description="( -(7/2), -(5/2) )"),
             models.Answer(description="Dos rectas nunca se intersectan"),
             models.Answer(description="Las rectas son paralelas, por lo tanto no se intersectan")],
    index_correct_answer=2
).save()

#15
models.Question(
    topics=[models.Topic(description='matematica')],
    text="La ecuación de la recta que pasa por los puntos (-2, 4) y (1, 2) es:",
    answers=[models.Answer(description="y = −(2/3)x + 8/3"),
             models.Answer(description="y = 2x"),
             models.Answer(description="y = (2/3)x + 8/3"),
             models.Answer(description="y = 2x + 8"),
             models.Answer(description="No se puede determinar la ecuación de una recta a partir de dos puntos")],
    index_correct_answer=0
).save()


soft_exports_statement = models.QuestionStatement(
    code="software_exports",
    text="Las siguiente pregunta es basada en el gráfico mostrado a continuación. \n"
         "[{image}] \n",
    image=models.Image(name="m1s6", data=images.img_m1_statement_16)
)

#16
models.Question(
    statement=soft_exports_statement,
    topics=[models.Topic(description='matematica')],
    text="De los datos mostrados en el gráfico se puede concluir:",
    answers=[models.Answer(description="Paraguay y Uruguay juntos tienen menor volumen de exportación que Argentina"),
             models.Answer(description="Paraguay y Uruguay juntos tienen mayor volumen de exportación que Argentina"),
             models.Answer(description="Paraguay y Uruguay juntos tienen igual volumen de exportación que Argentina"),
             models.Answer(description="Paraguay y Uruguay juntos tiene mayor volumen de exportaci´on que Brasil y "
                                       "Argentina juntos"),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=0
).save()

#17
models.Question(
    statement=soft_exports_statement,
    topics=[models.Topic(description='matematica')],
    text="De los datos mostrados en el gráfico se puede concluir:",
    answers=[models.Answer(description="Las exportaciones de Argentina en 2002 superan a las exportaciones sumadas las exportaciones"
                                       " de Paraguay entre el 2002 y 2005."),
             models.Answer(description="Las exportaciones de Uruguay superan por un factor mayor a 3 a las exportaciones de Paraguay"),
             models.Answer(description="Las exportaciones de Brasil en 2002 superan a las exportaciones sumadas las exportaciones"
                                       " de Paraguay entre el 2002 y 2005."),
             models.Answer(description="Las opciones a, b y c son verdaderas"),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=3
).save()

#18
models.Question(
    statement=soft_exports_statement,
    topics=[models.Topic(description='matematica')],
    text="Brasil exportó en 2004",
    answers=[models.Answer(description="83.000.000 US$."),
             models.Answer(description="21.000.000 US$"),
             models.Answer(description="5.000.000 US$"),
             models.Answer(description="21 US$"),
             models.Answer(description="210.000.000 US$")],
    index_correct_answer=1
).save()

#19
models.Question(
    topics=[models.Topic(description='matematica')],
    text=" La derivada de [{image}] es :",
    image=models.Image(name="m1q19", data=images.img_m1_question_19),
    answers=[models.Answer(description="[{image}]", image=models.Image(name="m1q19a1", data=images.img_m1_question_19_answer_1)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q19a2", data=images.img_m1_question_19_answer_2)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q19a3", data=images.img_m1_question_19_answer_3)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q19a4", data=images.img_m1_question_19_answer_4)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q19a5", data=images.img_m1_question_19_answer_5))],
    index_correct_answer=4
).save()

#20
models.Question(
    topics=[models.Topic(description='matematica')],
    text=" La integral de [{image}] es :",
    image=models.Image(name="m1q20", data=images.img_m1_question_20),
    answers=[models.Answer(description="[{image}]", image=models.Image(name="m1q20a1", data=images.img_m1_question_20_answer1)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q20a2", data=images.img_m1_question_20_answer2)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q20a3", data=images.img_m1_question_20_answer3)),
             models.Answer(description="[{image}]", image=models.Image(name="m1q20a4", data=images.img_m1_question_20_answer4)),
             models.Answer(description="Ninguna de las anteriores")],
    index_correct_answer=1
).save()

#21
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Dados los valores [{image}] y 2, se puede concluir:",
    image=models.Image(name="m1q21", data=images.img_m1_question_21),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=2
).save()

#22
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la ecuación 2x + 3 = x . Dados los valores x² y -9 podemos concluir:",
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#23
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Dados los valores 2^4 y 4^2 podemos concluir:",
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=2
).save()

#24
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Si n ∈ N (n es un número natural). Dados los valores 2^n y 2^(-n) podemos concluir:",
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=3
).save()

#25
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Conociendo El área de un círculo de radio A y El área de un cuadrado de lado 2A, se puede concluir que:",
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()

#26
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente figura: \n "
         "[{image}] \n"
         "Dados los valores 95º y la medida del angulo α, podemos concluir:",
    image=models.Image(name="m1q26", data=images.img_m1_question_26),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()

#27
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente figura: \n "
         "[{image}] \n"
         "Dados los valores: El perímetro del hexágono y 15m, podemos concluir:",
    image=models.Image(name="m1q27", data=images.img_m1_question_27),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#28
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente figura: \n "
         "[{image}] \n"
         "Dados los valores: La medida del lado a y 5m, podemos concluir:",
    image=models.Image(name="m1q28", data=images.img_m1_question_28),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=2
).save()

#29
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Dado El punto mínimo x de: -1 y\n"
         "f(x) = 4x 2 − 8x + 135 \n"
         "podemos concluir:",
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#30
models.Question(
    topics=[models.Topic(description='matematica')],
    text="Considerar la siguiente figura: \n "
         "[{image}]",
    image=models.Image(name="m1q30", data=images.img_m1_question_30),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor al primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

models.TestTemplate(description="Matematica M1",
                    question_quantity=30,
                    question_topics=[models.Topic(description="matematica")],
                    questions_per_topic=30).save()