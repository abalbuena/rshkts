from RSHKTS import models
from RSHKTS.queries import images

#Matematica 2

#1
models.Question(
    text="El máximo Común Divisor entre 12 y 2 es:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="6"),
             models.Answer(description="36"),
             models.Answer(description="12"),
             models.Answer(description="72"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=2
).save()


#2
models.Question(
    text="El Minimo Común Divisor entre 12 y 2 es:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="6"),
             models.Answer(description="36"),
             models.Answer(description="12"),
             models.Answer(description="72"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=3
).save()


#3
models.Question(
    text="Al simplificar 9 − 3[x − (3x + 2)] + 4 tenemos \n como resultado:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="6x + 19"),
             models.Answer(description="-6x"),
             models.Answer(description="6x"),
             models.Answer(description="6x - 19"),
             models.Answer(description="No se puede simplificar la expresion")
             ],
    index_correct_answer=0
).save()

#4
models.Question(
    text="La expresion algebraica (-5x^-2 * y) (-2x^-3 * y^2) es \n equivalente a:",
             topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="10x^5 / y^3"),
             models.Answer(description="10y^3 / x^5"),
             models.Answer(description="-10y^3 / x^5"),
             models.Answer(description="-10x^5 / y^3"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=1
).save()

#5
models.Question(
    text="La expresion algebraica xy - 5y - 2x es \n equivalente a:",
             topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="-xy + 5y + 2x - 10"),
             models.Answer(description="(x - 5)(y - 2)"),
             models.Answer(description="x - 5 +- 2x + 10"),
             models.Answer(description="-x + 5"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=1
).save()

#6
models.Question(
    text="Considerar la siguiente figura:\n [{image}] \n "
    "El valor de senx / cosx es igual a:",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q6", data=images.img_m2_question6),
    answers=[models.Answer(description="sena / senb"),
             models.Answer(description="cosa / cosb"),
             models.Answer(description="a / cb"),
             models.Answer(description="a / b"),
             models.Answer(description="ac / b")
             ],
    index_correct_answer=3
).save()

#7
models.Question(
    text="Considerar la siguiente ecuacion:\n "
    "tanx + 5 + x - 3 - cos^2x = sen^2x + senx / cosx",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="x = 1"),
             models.Answer(description="x = -1"),
             models.Answer(description="x = 2"),
             models.Answer(description="Ninguna de las anteriores"),
             models.Answer(description="La ecuacion no tiene solucion")
             ],
    index_correct_answer=1
).save()

#8
models.Question(
    text="La ecuacion que representa la frase:\n "
    "Doce es dieciseis menos que cuatro veces un \n numero \n es:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="4x - 16 = 12"),
             models.Answer(description="12 - 16 = 4x"),
             models.Answer(description="16 - 12 = 4x"),
             models.Answer(description="4x  - 16 = 12x"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=0
).save()

#9
models.Question(
    text="Considerar la siguiente ecuacion:\n 2^x = 64 \n La solucion es: ",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="x = 4"),
             models.Answer(description="x = 5"),
             models.Answer(description="x = 6"),
             models.Answer(description="x = 7"),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=2
).save()

#10
models.Question(
    text="Considerar la siguiente figura:\n [{image}] \n "
    "El volumen del cono recto de radio r = 5 y altura \n h = 2 es igual a:",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q10", data=images.img_m2_question10),
    answers=[models.Answer(description="10π"),
             models.Answer(description="10π/3"),
             models.Answer(description="50π/3"),
             models.Answer(description="10π/2"),
             models.Answer(description="50π/2")
             ],
    index_correct_answer=2
).save()

#11
models.Question(
    text="Considerar la siguiente figura:\n [{image}] \n "
    "El volumen del cuerpo es igual a:",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q11", data=images.img_m2_question11),
    answers=[models.Answer(description="36"),
             models.Answer(description="72"),
             models.Answer(description="18"),
             models.Answer(description="144"),
             models.Answer(description="11")
             ],
    index_correct_answer=0
).save()

#12
models.Question(
    text="Considerar la siguiente ecuacion de recta:\n "
    "y = 2x + 1\n"
    "Y la siguiente ecuacion de parabola:\n"
    "y = x^2 - 7\n"
    "Ambas curvas se interceptan en los puntos:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="Dos curvas nunca se pueden interceptar"),
             models.Answer(description="Una parabola y una recta nunca se pueden interceptar"),
             models.Answer(description="(4,9)"),
             models.Answer(description="(-2,-3)"),
             models.Answer(description="(4,9)y(-2,-3)")
             ],
    index_correct_answer=4
).save()

#13
models.Question(
    text="La distancia entre los puntos (1,1) y (2,-2) es:\n ",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="2"),
             models.Answer(description="2√5"),
             models.Answer(description="10"),
             models.Answer(description="√(10)"),
             models.Answer(description="No se puede calcular la distancia entre dos puntos")
             ],
    index_correct_answer=3
).save()

#14
models.Question(
    text="La simplificacion del siguiente termino:\n "
    "√(128 * a^2 * b^4) / √(16ab) es:",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="2√(2ab)"),
             models.Answer(description="√(2ab)"),
             models.Answer(description="b√(2ab)"),
             models.Answer(description="2b√(2ab)"),
             models.Answer(description="2ab")
             ],
    index_correct_answer=3
).save()

#15
models.Question(
    text="La simplificacion del siguiente termino:\n "
    "[{image}] \n es:",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q15", data=images.img_m2_question15),
    answers=[models.Answer(description="[{image}]", image=models.Image(name="m2q15a", data=images.img_m2_question15_a)),
             models.Answer(description="[{image}]", image=models.Image(name="m2q15b", data=images.img_m2_question15_b)),
             models.Answer(description="[{image}]", image=models.Image(name="m2q15c", data=images.img_m2_question15_c)),
             models.Answer(description="[{image}]", image=models.Image(name="m2q15d", data=images.img_m2_question15_d)),
             models.Answer(description="Ninguna de las anteriores")
             ],
    index_correct_answer=0
).save()

#empleados-statement 16, 17 y 18
empleados_statement = models.QuestionStatement(
    code="empleados_statement",
    text="Las siguientes tres preguntas son basadas en el \n grafico mostrado a continuacion:",
    image =  models.Image(name="m2q16", data=images.img_m2_question16_17_18)
)

#16
models.Question(
    statement = empleados_statement,
    text="El porcentaje de los empleados que actualmente \n"
    "no tienen pareja oficial (solteros, viudos y sepa- \n"
    "rados) es:",
    topics=[models.Topic(description="matematica")],
    answers=[
        models.Answer(description="50%"),
        models.Answer(description="52.5%"),
        models.Answer(description="37.5%"),
        models.Answer(description="47.5%"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=1
).save()

#17
models.Question(
    statement = empleados_statement,
    text="La cantidad total de empleados es:",
    topics=[models.Topic(description="matematica")],
    answers=[
        models.Answer(description="38"),
        models.Answer(description="39"),
        models.Answer(description="41"),
        models.Answer(description="42"),
        models.Answer(description="Ninguna de las anteriores")
    ],
    index_correct_answer=4
).save()

#18
models.Question(
    statement = empleados_statement,
    text="El 5% de los empleados es:",
    topics=[models.Topic(description="matematica")],
    answers=[
        models.Answer(description="Casado"),
        models.Answer(description="Viudo"),
        models.Answer(description="Divorciado"),
        models.Answer(description="Separado"),
        models.Answer(description="Concubinado")
    ],
    index_correct_answer=1
).save()

#19
models.Question(
    text="El valor del siguiente limite es: \n [{image}]",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q19", data=images.img_m2_question19),
    answers=[models.Answer(description="0"),
             models.Answer(description="1"),
             models.Answer(description="2"),
             models.Answer(description="∞"),
             models.Answer(description="No existe el limite")
             ],
    index_correct_answer=2
).save()

#20
models.Question(
    text="El valor del siguiente limite es: \n [{image}]",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q20", data=images.img_m2_question20),
    answers=[models.Answer(description="0"),
             models.Answer(description="1"),
             models.Answer(description="2"),
             models.Answer(description="∞"),
             models.Answer(description="No existe el limite")
             ],
    index_correct_answer=2
).save()

#21
models.Question(
    text="Dados los siguientes dos valores: \n "
    "(12^2 + 6) / 50 \n"
    "y \n"
    "2 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#22
models.Question(
    text="Dados los siguientes dos valores: \n "
    "a) log2 16 \n"
    "b) 5 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()

#23
models.Question(
    text="Teniendo en cuenta que x es cualquier numero real, \n dados los siguientes dos valores: \n "
    "a) x^2 - 2x + 1 \n"
    "b) (x - 1)^2 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=2
).save()

#24
models.Question(
    text="Considerando que los tres primeros terminos de una progresion \n,"
    "aritmetica son 3, 7 y 11 \n y dados los siguientes dos valores: \n "
    "a) El decimo termino de la progresion \n"
    "b) 39 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=2
).save()

#25
models.Question(
    text="Considerando que los tres primeros terminos de una progresion \n,"
    "geometrica son 5, 1/2 y 1/20 \n y dados los siguientes dos valores: \n "
    "a) El radio de la progresion \n"
    "b) 1/11 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#26
models.Question(
    text="Teniendo la siguiente figura \n [{image}] \n"
    "y si x es un angulo menor que 45 grados\n  dados los siguientes dos valores: \n "
    "a) senx \n"
    "b) cosx \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q26", data=images.img_m2_question26),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()

#27
models.Question(
    text="Considerar la siguiente ecuacion \n "
    "[{image}] \n"
    "Y dados los siguientes dos valores: \n "
    "a) -2 \n"
    "b) La solucion de la ecuacion \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q27", data=images.img_m2_question27),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()

#28
models.Question(
    text="Considerar el siguiente sistema de ecuaciones \n "
    "[{image}] \n"
    "Y en base al mismo elegir la respuesta correcta \n dados los siguientes dos valores: \n "
    "a) x + y \n"
    "b) x + z \n",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q28", data=images.img_m2_question28),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=0
).save()

#29
models.Question(
    text="Considerar el siguiente triangulo isoceles \n "
    "(no esta a escala). \n"
    "[{image}]"
    "\n Y dados los siguientes dos valores: \n "
    "a) 2a \n"
    "b) b \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q29", data=images.img_m2_question29),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=3
).save()


#30
models.Question(
    text="Considerar la siguiente figura \n "
    "[{image}]"
    "\n Y dados los siguientes dos valores: \n "
    "a) La suma de las areas de las 6 caras \n"
    "b) 96 \n"
    "Elegir la respuesta correcta",
    topics=[models.Topic(description="matematica")],
    image=models.Image(name="m2q30", data=images.img_m2_question30),
    answers=[models.Answer(description="El primer valor es mayor al segundo "),
             models.Answer(description="El segundo valor es mayor que el primero"),
             models.Answer(description="El segundo valor es igual al primero"),
             models.Answer(description="No se puede determinar cual de las cantidades es mayor")],
    index_correct_answer=1
).save()



models.TestTemplate(description="Matematica M2",
                    question_quantity=30,
                    question_topics=[models.Topic(description="matematica")],
                    questions_per_topic=30).save()