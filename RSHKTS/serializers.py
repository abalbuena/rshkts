from rest_framework_mongoengine.serializers import *
from rest_framework import serializers as drf_serializer
from RSHKTS import models

class TopicSerializer(EmbeddedDocumentSerializer):

    class Meta:
        model = models.Topic
        #fields = ('description')


class AnswerSerializer(EmbeddedDocumentSerializer):
    class Meta:
        model = models.Answer
        fields = ('description', 'image')
        depth = 2

class QuestionStatementSerializer(EmbeddedDocumentSerializer):
    class Meta:
        model = models.QuestionStatement
        fields = ('text', 'image')
        depth = 2

class QuestionSerializer(DocumentSerializer):
    topics = TopicSerializer(many=True)
    answers = AnswerSerializer(many=True)
    statement = QuestionStatementSerializer()
    class Meta:
        model = models.Question
        fields = ('text', 'topics', 'answers', 'image', 'statement')
        depth = 3


class TestTemplateSerializer(DocumentSerializer):
    class Meta:
        model = models.TestTemplate
        fields = ('id', 'description', 'question_quantity', 'question_topics', 'questions_per_topic', 'duration')
        depth = 2


class TemplatesStatusSerializer(EmbeddedDocumentSerializer):
    test_template = TestTemplateSerializer()
    class Meta:
        model = models.TemplatesStatus
        fields = ('test_template', 'state')


class CandidateSerializer(DocumentSerializer):
    current_test_templates = TemplatesStatusSerializer(many=True)
    class Meta:
        model = models.Candidate
        fields = ('id', 'name', 'id_number', 'current_test_templates')
        depth = 1


class TestSerializer(DocumentSerializer):
    questions = QuestionSerializer(many=True)
    candidate = CandidateSerializer()
    template = TestTemplateSerializer()
    class Meta:
        model = models.Test
        fields = ('id', 'candidate', 'questions', 'template', 'chosen_answers', 'final_score', 'questions_to_check', 'last_update', 'dateTime')
        depth = 4
