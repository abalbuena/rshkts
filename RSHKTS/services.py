from rest_framework.views import APIView

__author__ = 'horacio'
from RSHKTS import models, serializers
from rest_framework.generics import ListCreateAPIView
from rest_framework import renderers, status
from rest_framework.response import Response
from django.http.response import HttpResponseRedirect
from RSHKTS import domain
import logging
import json
logger = logging.getLogger(__name__)
from rest_framework.decorators import api_view
from django.core.mail import EmailMessage

# REST for Candidates
@api_view(['POST'])
def candidate_registration(request, format='json'):
    if request.method == 'POST':
        logger.debug("Handling POST request for candidates")
        logger.debug(request.data)
        try:
            candidate_name = request.data.get('name')
            candidate_id = str(request.data.get('id_number'))

            if candidate_id is None or candidate_name is None:
                raise ValueError("Invalid request parameters supplied: candidate_id, candidate_name",
                                 candidate_id, candidate_name)

            candidate = domain.get_candidate(candidate_name.strip().lower(), candidate_id.strip())
            response = serializers.CandidateSerializer(candidate)
            if candidate.existing:
                return Response(response.data, status=status.HTTP_409_CONFLICT)

            else:
                return Response(response.data, status=status.HTTP_201_CREATED)

        except Exception as e:
            logger.error(e, e.args)
            return Response({'message': e.args[0], 'args': e.args[1:]}, status=status.HTTP_400_BAD_REQUEST)


# REST for test_templates
@api_view(['GET'])
def template_list(request, format='json'):

    if request.method == "GET":
        logger.debug("GET request for test_templates")
        test_templates = models.TestTemplate.objects.all()
        response = serializers.TestTemplateSerializer(test_templates, many=True)
        return Response(response.data)

# REST for tests
@api_view(['POST', 'PUT'])
def manage_test(request, format='json'):

    if request.method == "POST":
        logger.debug("Handling POST request for tests")
        logger.debug(request.data)
        try:
            template_id = request.data.get('template_id')
            candidate_id = request.data.get('candidate_id')

            if candidate_id is None or template_id is None:
                raise ValueError("Invalid request parameters supplied: candidate_id, candidate_name",
                                 candidate_id, template_id)

            test = domain.get_test(template_id.strip(), candidate_id)
            response = serializers.TestSerializer(test)
            return Response(response.data, status=status.HTTP_200_OK)

        except Exception as e:
            logger.error(e)
            return Response({'message': e.args[0], 'args': e.args[1:]}, status=status.HTTP_400_BAD_REQUEST)

    # from put requests, parameters goes into req body, we get them with request.query_param as QueryDictionary
    if request.method == "PUT":
        logger.debug("Handling PUT request for tests")
        logger.debug('data : ' + json.dumps(dict(request.data)))
        try:
            test_id = request.data.get('test_id').strip()
            answers = request.data.get('answers')
            state = request.data.get('state').strip()
            questions_to_check = request.data.get('questions_to_check')

            updated_test = domain.update_test(test_id, answers, state, questions_to_check)
            response = serializers.TestSerializer(updated_test)
            return Response(response.data, status=200)

        except Exception as e:
            logger.error(e, e.args)
            return Response({'message : ': e.args[0], 'code':e.args[1], 'args : ': e.args[2:]}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def send_mail(request, format='json'):
    if request.method == "POST":
        logger.debug("Handling PUT request for send_mail")
        logger.debug('data : ' + json.dumps(dict(request.data)))
        try:
            candidate_id = request.data.get('candidate_id')
            candidate = models.Candidate.objects.filter(id=candidate_id).get(0)
            candidate_tests = serializers.TestSerializer(models.Test.objects.filter(candidate=candidate_id), many=True).data
            message = "Candidato : " + candidate.name + " \n"
            for test in candidate_tests:
                message += 'Examen : ' + test['template']['description'] + ' Puntaje : ' + str(test['final_score'])+ ' \n '
            email = EmailMessage('Examen', message, 'benitezho@gmail.com', ['pschmeda@roshka.com', 'scantero@roshka.com', 'hbenitez@roshka.com'])
            email.send()
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            logger.error(e, e.args)
            return Response({'message:' : e.args[0]},status=status.HTTP_500_INTERNAL_SERVER_ERROR)