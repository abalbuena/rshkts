var rshkts = angular.module('rshkts', ['ngRoute', 'services', 'ngDialog','timer'])
    .config(function($routeProvider, ngDialogProvider) {
        var base_url = 'static/html/'
        $routeProvider
            .when('/', {
                templateUrl: base_url.concat('candidate-register.html'),
                controller: 'candidateController'
            })
            .when('/templates', {
                templateUrl: base_url.concat('user-templates.html'),
                controller: 'userTemplatesController'
            })
            .when('/test_summary', {
                templateUrl: base_url.concat('test-summary.html'),
                controller: 'testSummaryController'
            })
            .when('/contact', {
                templateUrl: 'page-contact.html',
                controller: 'contactController'
            })
            .when('/test', {
                templateUrl: base_url.concat('test.html'),
                controller: 'testController'
            })
            .when('/finished', {
                templateUrl: base_url.concat('final-score.html'),
                controller: 'finishedController'
            })
            .when('/widget-time', {
                templateUrl: base_url.concat('widget-time.html'),
                controller: 'widgetController'
            });

        ngDialogProvider.setDefaults({
			className: 'ngdialog-theme-plain',
			plain: false,
			showClose: true,
			closeByDocument: true,
			closeByEscape: true,
			appendTo: false
		});
    });

rshkts.controller('candidateController', ['$scope', '$http', 'rshktsService', 'ngDialog', function($scope, $http, rshktsService, ngDialog) {
    $scope.pageClass = 'page-home';
    $scope.categories = [];

    // ahora los templates se cargan en la siguiente pantalla.

    // mudar esto al modulo rshkts.services
    $scope.submitRegister = function () {
        // Simple POST request example (passing data) :
        $http.post('/RSHKTS/serv/candidates.json', {
            name: $scope.nombreCandidato,
            id_number: $scope.cedula
        }).
            success(function (data, status, headers, config) {
                rshktsService.addCandidate(data);
                rshktsService.changeView("templates");
            }).
            error(function (data, status, headers, config) {
                if(status === 409){
                    console.log("se capturó el 409");
                    //window.alert("Usuario ya existe, se recuperará el examen.");
                    rshktsService.addCandidate(data);
                    rshktsService.isExistingCandidate = true;
                    $scope.candidateName = data.name;
                    ngDialog.open({
			            template: 'existentCandidateDialog',
                        scope : $scope
		            }).closePromise.then(function(){
                        rshktsService.changeView("templates");
                    });
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

    };
}]);

/**
 * Contolador para vista de todos los templates.
 */
rshkts.controller('userTemplatesController', ['$scope', '$rootScope', 'rshktsService', 'ngDialog', function ($scope, $rootScope, rshktsService, ngDialog) {
    rshktsService.getTestsTemplates(function(allTemplates) {
        rshktsService.getUserTemplates(function(userTemplates){
            $scope.templates = [];

            allTemplates.forEach(function(template, index) {
                //crea un nuevo objeto para crear propiedades personalizadas.
                var templateViewModel = {'id': template.id,
                                         'description': template.description,
                                         'question_quantity': template.question_quantity,
                                         'question_topics': template.question_topics,
                                         'questions_per_topic': template.questions_per_topic,
                                         'duration': template.duration};

                userTemplates.forEach(function(userTemplate, index) {
                    templateViewModel.status = 'n';
                    if(userTemplate.test_template.id === template.id) {
                        templateViewModel.status = userTemplate.state;
                    }
                });

                $scope.templates.push(templateViewModel);
            })
        })
    });

    $scope.selectTemplate = function(templateId) {
        // para verfificar el estado del template seleccionado.
        var templateData = $scope.templates.filter(function (element, index) {
                if (element.id === templateId) {
                    return element;
                }
            });

        templateData = templateData[0];

        if(templateData.status === 'f') {
            ngDialog.open({
                template: 'testAlreadyFinished',
                scope : $scope
            }).closePromise.then(function(){
                rshktsService.changeView("templates");
            });
        } else {
            rshktsService.setTestTemplate(templateId);
            rshktsService.changeView("test_summary");
        }
    }

    $scope.sendEmailToAdmin = function () {
        rshktsService.sendEmailToAdmin();
        rshktsService.changeView("")
    }
}])

rshkts.controller('testSummaryController', ['$scope', '$rootScope', 'rshktsService', 'ngDialog', '$http', function($scope, $rootScope, rshktsService, ngDialog, $http) {

    var testTemplate = rshktsService.getTestTemplate();
    var candidate = rshktsService.getCandidate();

    $scope.testName = testTemplate.description;
    $scope.testDuration = testTemplate.duration;
    $scope.testLength = testTemplate.question_quantity;

    $scope.testTopics = testTemplate.question_topics;
    $scope.candidateName = candidate.name;
    $scope.candidateId = candidate.id;

    $scope.startTest = function () {
        // Simple POST request example (passing data) :
        $http.post('/RSHKTS/serv/test', {
            candidate_id: $scope.candidateId,
            template_id: !rshktsService.isExistingCandidate? testTemplate.id : undefined
        }).success(function (data, status, headers, config) {
                rshktsService.setTestContent(data);
                rshktsService.changeView("test");
            }).
            error(function (data, status, headers, config) {
                ngDialog.open({
			            template: 'testAlreadyFinished'
		            });
            });

    }
}]);

rshkts.controller('testController', function($scope, rshktsService, $http, $interpolate, $sce) {
    var test = rshktsService.getTestContent();
    var testTemplate = rshktsService.getTestTemplate();
    $scope.currentQuestion = rshktsService.getCurrentQuestion();
    $scope.totalQuestions = test.questions.length;

    //TODO: se convierte de HORAS a segundos, cambiar en caso que finalmente se devuelva la duración en minutos.
    var fechaInicio = new Date(test.dateTime);
    var lastUpdate = new Date(test.last_update || test.dateTime);
    var tiempoRendido = lastUpdate.getTime() - fechaInicio.getTime();
    $scope.testDuration = (testTemplate.duration * 60) - parseInt((tiempoRendido/1000)/60);

    // para armar el índice de preguntas.
    function questionsIndexGenerator(test) {
        $scope.questionsIndex = [];
        for(var i = 0; i < test.questions.length; i++) {
            var classes = [];

            if(test.questions_to_check.indexOf(i) > -1){
                classes.push("pending");
            }

            if (test.chosen_answers[i] != -1) {
                classes.push("marked");
            }

            if(i === $scope.currentQuestion) {
                classes.push("current");
            }

            $scope.questionsIndex.push({'index': i , 'desc' : i + 1, 'class' : classes.join(" ") });
        }
    }

    ///preguntas sin responder, para el widget.
    $scope.remainingQuestions=test.chosen_answers.filter(function(element,index){if(element==-1)return element}).length;

    ///se comprueba si es un postulante ya existente.
    if(rshktsService.isExistingCandidate) {
        rshktsService.setCurrentQuestion(test.chosen_answers.indexOf(-1))
    }
    else {
        rshktsService.setCurrentQuestion(0);
    }

    // cambia los valores del $scope con los de la pregunta seleccionada.
    var renderQuestion = function(index){
        if(index != undefined){
            $scope.currentQuestion = index;
        }
        else {
            $scope.currentQuestion = rshktsService.getCurrentQuestion();
        }
        //'id', 'candidate', 'questions', 'template', 'final_score', 'chosen_answers'
        //currentQuestion = rshktsService.getCurrentQuestion();

        var statementHTML = '';

        if (test.questions[$scope.currentQuestion].statement) {
            statementHTML += test.questions[$scope.currentQuestion].statement.text;

            if (test.questions[$scope.currentQuestion].statement.image) {
                $scope.imageHTML = '<img src="' + test.questions[$scope.currentQuestion].statement.image.data + '" />';
                statementHTML = statementHTML.replace('[{image}]', '{{ imageHTML }}');
            }

            statementHTML = $interpolate(statementHTML)($scope);
        }

        statementHTML += test.questions[$scope.currentQuestion].text;

        //formatea los saltos de linea.
        statementHTML = statementHTML.replace(/\n/g, "<br/>");

        if(test.questions[$scope.currentQuestion].image) {
            $scope.imageHTML = '<img src="' + test.questions[$scope.currentQuestion].image.data + '" />';
            statementHTML = statementHTML.replace('[{image}]', '{{ imageHTML }}');

            statementHTML = $interpolate(statementHTML)($scope);
        }

        $scope.question = statementHTML;

        //$scope.questionimage = test.questions[$scope.currentQuestion].image.data;
        $scope.questionId = $scope.currentQuestion;

        $scope.selectedAnswer = test.chosen_answers[$scope.currentQuestion];


        //if(test.chosen_answers[$scope.currentQuestion] != -1)
        //{
        //    $scope.formData.selectedAnswer = test.chosen_answers[$scope.currentQuestion];
        //}

        $scope.answers = test.questions[$scope.currentQuestion].answers.map(function(element, index){
            //var checked = ($scope.selectedAnswer == index) ? "checked" : "";
            var answerHTML = '';

            if (element.description) {
                answerHTML = element.description.replace(/\n/g, "<br/>");
            }

            if(element.image) {
                $scope.optionImageHTML = '<img src="' + element.image.data + '" />';
                answerHTML = answerHTML.replace('[{image}]', '{{ optionImageHTML }}');

                answerHTML = $interpolate(answerHTML)($scope);
            }

            return { 'answerHTML' : answerHTML};
        });

        $scope.checkLater = false;
        $scope.isFinalQuestion = ($scope.currentQuestion + 1) === test.questions.length;
        $scope.questionsToCheckLater = test.questions_to_check;

        /*
        * asigna la respuesta ya seleccionada.
        */
        $scope.formData = {selectedAnswer: $scope.selectedAnswer};

        rshktsService.setCurrentQuestion($scope.currentQuestion);

        questionsIndexGenerator(test);
    };

    $scope.renderQuestion = renderQuestion;

    $scope.isSelected = function(index){
        return ( index == $scope.selectedAnswer )
    };

    ///envía una respuesta al servicio.
    $scope.sendAnswer = function() {
        /// si no es la respuesta final, se envía al rshktsService.
        if(!$scope.isFinalQuestion){
             test = rshktsService.getTestContent();
             rshktsService.saveAnswer($scope.formData.selectedAnswer,
                    $scope.formData.checkLater,
                    function(data) {
                        test = data;
                        renderQuestion();
                    }

             );
        }
        else {
            rshktsService.finalizeTest($scope.formData.selectedAnswer, function (data){
                $scope.test_submit_status = data;
                rshktsService.changeView("templates");
            });
        }
    };

    renderQuestion();

    //Definimos la función que se llama al finalizar el tiempo.
    $scope.callbackTimerFinished = function (){
        rshktsService.finalizeTest($scope.formData.selectedAnswer);
        rshktsService.changeView("finished");
    };

    //Esto inicia el contador.
    $scope.$broadcast('timer-start');
    $scope.timerRunning = true;

    //TODO: no usamos esta función, ya que el timer se inicia automáticamente.
    $scope.startTimer = function (){
        $scope.$broadcast('timer-start');
        $scope.timerRunning = true;
    };

    //TODO: ¿agregar pausas?
    $scope.stopTimer = function (){
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = false;
    };
    //TODO: en caso de agregar pausas, implementar esta función.
    $scope.$on('timer-stopped', function (event, data){
        console.log('Timer Stopped - data = ', data);
    });

    /*
    * El timer-tick es un evento que se lanza en un intervalo de tiempo.
    * Dicho intervalo se encuentra definito en el elemento HTML del timer.
    * Como nuestro intervalo es de 1 segundo, acumulamos 7 antes de hacer el backup del test.
    */
    var updateCounter = 0;
    $scope.$on('timer-tick', function (event, args) {
        updateCounter +=1;

        $scope.remainingQuestions = rshktsService.getPendingQuestions();
        if (updateCounter == 7){
            rshktsService.backUpAnswers();
            updateCounter = 0;
        }
    });

    $scope.renderHTML = function (html) {
        return $sce.trustAsHtml(html);
    }
});


rshkts.controller('finishedController', function($scope, rshktsService, $http) {
    var test = rshktsService.getTestContent();
    $scope.finalScore = test.final_score;
    $scope.candidateName = rshktsService.getCandidate().name;

    $scope.finish = function () {
        rshktsService.changeView("templates");
    }
});

//TODO: este controller no se está implementando.
rshkts.controller('widgetController', function($scope, rshktsService, $http) {
    $scope.timerRunning = true;

    $scope.startTimer = function (){
        $scope.$broadcast('timer-start');
        $scope.timerRunning = true;
    };

    $scope.stopTimer = function (){
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = false;
    };

    $scope.$on('timer-stopped', function (event, data){
        console.log('Timer Stopped - data = ', data);
    });

    $inject = ['$scope'];
});
