/**
 * Created by angel on 2/28/2015.
 */
angular.module('services',[]).service('rshktsService', function($location, $http) {
        var storage = {}
        var candidate;
        var test_templates;
        var templateData;
        var selectedTemplate;
        var test;
        var currentQuestion;
        var isExistingCandidate;
        var pendingQuestions;

        var saveStorage = function(key, value) {
            //sessionStorage.storage = sessionStorage.storage || {};
            //angular.extend(sessionStorage.storage, property);
            //sessionStorage.storage[key] = JSON.stringify(value);
            sessionStorage[key] = JSON.stringify(value);
        }

        var getStorage = function(key) {
            return sessionStorage.getItem(key) ? JSON.parse(sessionStorage.getItem(key)) : null;
        }

        // guarda la lista de TestTemplates, obtenidos del servicio.
        var saveTestTemplates = function (testTemplates) {
            saveStorage('test_templates',testTemplates);
        }

    /**
     *
     * @param callback - function (data) funcion a ejecutar luego de obtener los datos.
     */
    var getTestsTemplates = function (callback) {
        var testTemplatesFromStorage = getStorage("test_templates");

        if(!testTemplatesFromStorage) {
            $http.get("/RSHKTS/serv/test_templates")
                .success(function (data, status, headers, config) {
                    saveTestTemplates(data);
                    callback.call(null, data);
                    //return data;
                })
                .error(function (data, status, headers, config) {
                    console.error("No se pudieron obtener los templates de examen")
                });//.then(function (response) {
        } else {
            //return JSON.parse(testTemplatesFromStorage);
            callback.call(null, testTemplatesFromStorage);
            //return testTemplatesFromStorage;
        }

    }

        // guarda el candidato generado que retorna el servicio, .
    var addCandidate = function (newCandidate) {
        var userData = {
            'id': newCandidate.id,
            'name': newCandidate.name,
            'document_number': newCandidate.id_number
        }

        saveStorage('candidate', userData);
        saveStorage('userTestTemplates', newCandidate.current_test_templates);
    }


    var updateUserTestTemplates = function (userTestTemplates) {
        saveStorage('userTestTemplates', userTestTemplates);
    }

    // obtiene la lista de templates del usuario.
    var getUserTemplates = function(callback) {
        var userTemplates = getStorage("userTestTemplates");
        callback.call(null, userTemplates);
    }
    //guarda el template seleccionado por el usuario.
    var setTestTemplate = function(templateId) {
        getTestsTemplates(function(testTemplates){
            var templateData = testTemplates.filter(function (element, index) {
                if (element.id === templateId) {
                    return element;
                }
            })

            saveStorage('selectedTemplate', templateData[0]);
        });
    }
        // obtiene el candidato.
    var getCandidate = function () {
        return getStorage("candidate");
    }
        // obtiene los datos del TestTemplate seleccionado.
    var getTestTemplate = function () {
        var selectedTemplate = getStorage('selectedTemplate');
        if (!selectedTemplate) {
            return null;
        }
        return selectedTemplate;
    }

    // Guarda los datos del test.
    var setTestContent = function(testContent){
        //test = testContent;
        saveStorage('test', testContent);
        updatePendingQuestions();
    }

    // Retorna los datos del Test.
    var getTestContent = function(){
        return getStorage("test");
    }

    /// esto guarda LOCALMENTE la respuesta.
    var saveAnswer = function(answer, checkLater, callback){
        var test = getTestContent();
        var currentQuestion = getCurrentQuestion();

        test.chosen_answers[currentQuestion] = typeof answer !== "undefined" ? parseInt(answer) : -1;

        if(checkLater){
            test.questions_to_check.push(currentQuestion);
        }

        saveStorage('test', test);
        sendAnswer("a", function(data) {
            updateUserTestTemplates(data.candidate.current_test_templates);
            if(callback) callback.call(null, data);
        });
        setCurrentQuestion(currentQuestion += 1);

        return true;
    }

    var backUpAnswers = function(){
        sendAnswer("a");
        return true;
    }

    var finalizeTest = function (answer, callback) {
        var test = getTestContent();
        var currentQuestion = getCurrentQuestion();

        test.chosen_answers[currentQuestion] = typeof answer !== "undefined" ? parseInt(answer) : -1;

        saveStorage('test', test);
        sendAnswer("f", function (data) {
            updateUserTestTemplates(data.candidate.current_test_templates);
            if (callback) callback.call(null, data);
        });

        return true;
    }

    // para actualizar las respuestas en el servidor.
    var sendAnswer = function(state, callback){
        var test = getTestContent();

        $http.put('/RSHKTS/serv/test', {
            test_id: test.id,
            answers: test.chosen_answers.map(function(element, index) {
                return typeof element !== "undefined" && element !== null ? element : -1;
            }),
            state: state,
            questions_to_check : test.questions_to_check
        }).
        success(function (data, status, headers, config) {
            saveStorage('test', data);
            updatePendingQuestions();

            if(callback) return callback(data);

            return data;
        }).
        error(function (data, status, headers, config) {
            console.error("no se pudo enviar la respuesta..." + status);
            return false;
        });
    }

    var getCurrentQuestion = function(){
        return getStorage("currentQuestion");
    }

    var setCurrentQuestion = function(index){
        saveStorage('currentQuestion', index);
    }

    var updatePendingQuestions = function () {
        var test = getStorage('test');
        var pendingQuestions = test.chosen_answers.filter(function(element, index){if(element==-1) return element}).length;
        saveStorage("pendingQuestions", pendingQuestions);
    }

    var getPendingQuestions = function() {
        return getStorage("pendingQuestions");
    }

    // para cambiar la url, esto es manejado por el $routeProvider, definido en el config del modulo.
    var changeView = function (view) {
        $location.path(view);
    }

    var saveState = function() {

    }

    /**
     * Envia un mail para avisar que se termino.
     * @param callback - function funcion a ejecutar luego de enviar el mail.
     */
    var sendEmailToAdmin = function (callback) {
        var candidate = getCandidate();
        $http.post('/RSHKTS/serv/send_mail', {'candidate_id': candidate.id }).
        success(function (data, status, headers, config) {
        }).
        error(function (data, status, headers, config) {
            console.error("no se pudo enviar el mail al admin..." + status);
        }).then(function (data, status, headers, config) {
            console.error("ejecutar el callback de todos modos...");
            //llama a un callback en caso que exista.
            if (callback) callback.call(null);
        });
    }

    return {
        getUserTemplates: getUserTemplates,
        saveTestTemplates: saveTestTemplates,
        getTestsTemplates: getTestsTemplates,
        getTestTemplate: getTestTemplate,
        addCandidate: addCandidate,
        getCandidate: getCandidate,
        setTestTemplate: setTestTemplate,
        setTestContent: setTestContent,
        getTestContent: getTestContent,
        saveAnswer: saveAnswer,
        backUpAnswers: backUpAnswers,
        finalizeTest: finalizeTest,
        getCurrentQuestion: getCurrentQuestion,
        setCurrentQuestion: setCurrentQuestion,
        getPendingQuestions: getPendingQuestions,
        sendEmailToAdmin: sendEmailToAdmin,
        changeView: changeView
    };
});