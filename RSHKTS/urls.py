from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings
from django.conf.urls.static import static

from RSHKTS import views, services

urlpatterns = patterns('',
                       url(r'^RSHKTS/$', views.IndexView.as_view({'get': 'show_index'}), name='index'),
                       url(r'^RSHKTS/serv/candidates$', services.candidate_registration, name='candidates'),
                       url(r'^RSHKTS/serv/test_templates$', services.template_list, name='test_templates'),
                       url(r'^RSHKTS/serv/test$', services.manage_test, name='test'),
                       url(r'^RSHKTS/serv/send_mail$', services.send_mail, name='send_mail'),
                       url(r'^admin/', include(admin.site.urls))) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html', 'png'])