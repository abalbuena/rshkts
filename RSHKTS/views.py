from rest_framework.response import Response
from rest_framework import generics, viewsets, renderers
from RSHKTS import models
import logging

logger = logging.getLogger(__name__)


class IndexView(viewsets.ModelViewSet):

    def show_index(self, request, format=None, *args, **kwargs):
        return Response(template_name="index.html")
